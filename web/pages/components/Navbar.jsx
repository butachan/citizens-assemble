import Link from 'next/link';
import Image from 'next/image'

export const Navbar = () => {
    return (
        <>
            <nav className="shadow-lg">
                <Link href='/' className="flex justify-between w-full items-center flex-wrap bg-white p-3">
                    <a className='flex items-center py-2 px-5 justify-between w-screen'>
                        <Image src="/logo.png" alt={"logo Re(sources)"}
                               width={220}
                               height={64}
                        />
                        <div className="hidden lg:flex gap-4 items-center">
                            <span className='text-black font-bold uppercase tracking-wide'>Aide</span>
                            <span className='text-black font-bold uppercase tracking-wide'>FAQ</span>
                            <span className='text-black font-bold uppercase tracking-wide'>À propos</span>
                            <button className='rounded-full border-2 border-white hover:border-teal-600 hover:bg-transparent py-2 px-3 text-white hover:text-black font-bold uppercase tracking-wide' style={{backgroundColor: "#03989E"}}>
                                Se connecter
                            </button>
                            <button className='rounded-full border-2 border-teal-600 hover:bg-teal-600 py-2 px-3 text-black hover:text-white font-bold uppercase tracking-wide'>
                                Créer son profil
                            </button>
                        </div>

                        <div className="flex lg:hidden cursor-pointer">
                            <svg xmlns="http://www.w3.org/2000/svg" className="h-8 w-8" fill="none" viewBox="0 0 24 24"
                                 stroke="currentColor">
                                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"
                                      d="M4 6h16M4 12h16M4 18h16"/>
                            </svg>
                        </div>
                    </a>
                </Link>
            </nav>
        </>
    )
}
