// posts will be populated at build time by getStaticProps()
import axios from "axios";

function Category(posts) {
    return (
        <ul>
            {posts}
        </ul>
    )
}

// This function gets called at build time on server-side.
// It won't be called on client-side, so you can even do
// direct database queries.
export async function getStaticProps(context) {
    const {data} = await axios.get('http://127.0.0.1:8000/api/categories')
    return {data}
}


export default Category
