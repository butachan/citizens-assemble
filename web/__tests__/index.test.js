import React, { useState } from "react";
import { getServerSideProps } from "../pages/index";
import Resource from "../pages/index"
import "@testing-library/jest-dom";
import { fireEvent, render, screen, waitFor } from "@testing-library/react";
import jestFetchMock from "jest-fetch-mock";


const resources =  "http://127.0.0.1:8000/api/resources";
const users = "http://127.0.0.1:8000/api/users";

describe("getServerSideProps", () => {

  beforeEach(() => {
    jestFetchMock.resetMocks();
  });


  it("should call api", async () => {
    jestFetchMock.mockResponseOnce(
      JSON.stringify({ resources, users })
    );


    const response = await getServerSideProps();
    (
          expect.objectContaining({
            props: {
              resources: {},
              users: {}
            }
          })
          );
  });
});

