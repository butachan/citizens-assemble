import React from "react";
import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import {Footer} from "../pages/components/Footer";

test('should navigate to ... when link is clicked', () => {
    render(<Footer/>)
    expect(screen.getByText('www.solidarites-sante.gouv.fr', {exact:false}).closest('a')).toHaveAttribute('href', 'https://solidarites-sante.gouv.fr/')
});