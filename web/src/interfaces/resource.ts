export interface Resource {
  "@id"?: string;
  name?: string;
  description?: string;
  image?: string;
  activatedAt?: Date;
  type?: any;
}
