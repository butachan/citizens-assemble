import axios from "axios";

//TODO utiliser le .env pour ne pas hardcoder l'url. process.env.BASEURL

const api = () => {
    const client = axios.create({
        baseURL: "http://127.0.0.1:8000/api/"
    })
    client.interceptors.request.use(
        config => {
            config.headers = {Accept: 'application/json'}
            return config
        },
        error => {
            Promise.reject(error)
        }
    )

    return client
}

export default api()
