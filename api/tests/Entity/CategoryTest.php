<?php declare(strict_types=1);

namespace App\Tests\Entity;

use App\Entity\Category;
use App\Entity\Resource;
use PHPUnit\Framework\TestCase;

final class CategoryTest extends TestCase
{
    /** @test */
    public function testAddResource()
    {
        $category = new Category();
        $resource = new Resource();
        $category->addResource($resource);

        $this->assertContains($resource, $category->getResources());
    }
    /** @test */
    public function testRemoveResource()
    {
        $category = new Category();
        $resource = new Resource();
        $resource->setCategory($category);
        $category->removeResource($resource);

        $this->assertNotContains($resource, $category->getResources());
    }
}