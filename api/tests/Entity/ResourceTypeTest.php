<?php declare(strict_types=1);

namespace App\Tests\Entity;

use App\Entity\Resource;
use App\Entity\ResourceType;
use PHPUnit\Framework\TestCase;

final class ResourceTypeTest extends TestCase
{
    /** @test */
    public function testAddResource()
    {
        $resource = new Resource();
        $resourceType = new ResourceType();
        $resourceType->addResource($resource);

        $this->assertContains($resource, $resourceType->getResources());
    }

    /** @test */
    public function testRemoveResource()
    {
        $resource = new Resource();
        $resourceType = new ResourceType();
        $resourceType->addResource($resource);
        $resourceType->removeResource($resource);

        $this->assertNotContains($resource, $resourceType->getResources());
    }

}