<?php declare(strict_types=1);

namespace App\Tests\Entity;

use App\Entity\User;
use PHPUnit\Framework\TestCase;

final class UserTest extends TestCase
{
    /** @test */
    public function testGetEmail()
    {
        $user = new User();
        $user->setEmail('john.doe@test.com');

        $this->assertEquals('john.doe@test.com', $user->getEmail());
    }

}