<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use App\Entity\User;
use Symfony\Component\Validator\Constraints\DateTime;

class UserFixtures extends Fixture  implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        for ($i = 0; $i < 10; $i++) {
            $user = new User();
            $user->setImage('https://images.unsplash.com/photo-1633332755192-727a05c4013d?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=580&q=80');
            $user->setFirstName('Prénom'.$i);
            $user->setLastName('Nom'.$i);
            $user->setEmail('email.'.$i .'@gmail.com');
            $user->setPassword('password');
            $user->setCity('Bordeaux');
            $user->setCountry('France');
            $user->setZipCode('33000');
            $user->setUpdatedAt(new \DateTimeImmutable());
            $user->setCreatedAt(new \DateTimeImmutable());

            $manager->persist($user);
        }
        $manager->flush();

    }

    public function getOrder(): int
    {
        return 4;
    }
}
