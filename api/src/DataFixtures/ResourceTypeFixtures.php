<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use App\Entity\ResourceType;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ResourceTypeFixtures extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        /*
         *ResourceType 1
         */

        $resource1= new ResourceType();
        $resource1-> setLabel('Vidéo');

        $manager->persist($resource1);
        $manager->flush();

        /*
         *ResourceType 2
         */

        $resource2= new ResourceType();
        $resource2->setLabel('Jeu');

        $manager->persist($resource2);
        $manager->flush();

        /*
         *ResourceType 3
         */

        $resource3= new ResourceType();
        $resource3->setLabel('Livre');

        $manager->persist($resource3);
        $manager->flush();

        /*
         *ResourceType 4
         */

        $resource4= new ResourceType();
        $resource4->setLabel('Conférence');

        $manager->persist($resource4);
        $manager->flush();

        /*
         *ResourceType 5
         */

        $resource5= new ResourceType();
        $resource5->setLabel('Défi');

        $manager->persist($resource5);
        $manager->flush();

        /*
         *ResourceType 6
         */

        $resource6= new ResourceType();
        $resource6->setLabel('Atelier');

        $manager->persist($resource6);
        $manager->flush();

        /*
         *ResourceType 7
         */

        $resource7= new ResourceType();
        $resource7->setLabel('Autre');

        $manager->persist($resource7);
        $manager->flush();

        /*
         *ResourceType 8
         */

        $resource8= new ResourceType();
        $resource8->setLabel('Vidéo');

        $manager->persist($resource8);
        $manager->flush();

        /*
         *ResourceType 9
         */

        $resource9= new ResourceType();
        $resource9->setLabel('Défi');

        $manager->persist($resource9);
        $manager->flush();

        /*
         *ResourceType 10
         */

        $resource10= new ResourceType();
        $resource10->setLabel('Livre');

        $manager->persist($resource10);
        $manager->flush();

        /*
         *ResourceType 11
         */

        $resource11= new ResourceType();
        $resource11->setLabel('Vidéo');

        $manager->persist($resource11);
        $manager->flush();

        /*
        *ResourceType 12
        */

        $resource12= new ResourceType();
        $resource12->setLabel('Défi');

        $manager->persist($resource12);
        $manager->flush();

        /*
         *ResourceType 13
         */

        $resource13= new ResourceType();
        $resource13->setLabel('Livre');

        $manager->persist($resource13);
        $manager->flush();

    }
    public function getOrder(): int
    {
        return 0;
    }
}